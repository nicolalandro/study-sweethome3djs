# Study SweetHome3D JS
This is a study of SweetHome3D js and how to use it.

## How to run php

```
docker-compose build
docker-compose up

# php at localhost:8081
```

## How to run .war

```
# download SweetHome3DJS-7.2.war and put into jsp folder from https://sourceforge.net/projects/sweethome3d/files/SweetHome3DJS/

docker-compose build
docker-compose up

# tomcat at http://localhost:8080/SweetHome3DJS-7.2/
```

## How to run build container [Not working]

```
cd build
docker-compose build
docker-compose run sweethome3djs bash
$ ant transpiledLibraries
$ ant viewerLibraries # error
$ ant
```

# References
* Sweet home 3D:
  * [SweetHome3DJS](https://sourceforge.net/projects/sweethome3d/files/SweetHome3DJS-source/): Web version of sweet home 3D
    * [forum](http://www.sweethome3d.com/support/forum/viewthread_thread,10403_offset,0): info forum
    * [Builded](https://sourceforge.net/projects/sweethome3d/files/SweetHome3DJS/)
    * [forum](http://www.sweethome3d.com/support/forum/viewthread_thread,12791_lastpage,yes#60399): info asked about build and other things
  * [Mobile](https://www.sweethome3d.com/blog/2023/09/06/sweet_home_3d_mobile_app.html): mobile article of sweet home 3D
  * [SH3DO](https://github.com/cincheo/sh3do-getting-started): a way to implement SweetHome3D
* DEV
  * [docker-ant](https://github.com/comses/docker-ant/blob/main/Dockerfile): docker file example for ant
  * [docker tompcat](https://github.com/saverioriotto/docker-tomcat-sample/tree/master)