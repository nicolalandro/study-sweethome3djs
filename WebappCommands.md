# How to use frontend by code

This file explain how to use programmatically the frontend. You can use that commands from the browser console.

## Create wall
http://www.sweethome3d.com/javadoc/com/eteks/sweethome3d/model/Wall.html
```
# get home walls
window.application.getHomes()[0].walls

# params: x1, y1 x2, y2, spessore, altezza
window.application.getHomes()[0].addWall(new Wall(0, 180, 0, 600, 20, 250)) 
```

## Create room (pavimento)
http://www.sweethome3d.com/javadoc/com/eteks/sweethome3d/model/Room.html

```
# get home rooms
window.application.getHomes()[0].rooms

# params: [ [float, float], ... ]
window.application.getHomes()[0].addRoom(new Room([[0,0], [0,100], [100, 100], [100, 0]]))

# params: id,  [ [float, float], ... ]
window.application.getHomes()[0].addRoom(new Room("1", [[0,0], [0,100], [100, 100], [100, 0]]))

# to not show roof
room = new Room([[0,0], [0,100], [100, 100], [100, 0]])
room.setCeilingVisible(false)
window.application.getHomes()[0].addRoom(room)
```

## Add Furniture
http://www.sweethome3d.com/javadoc/com/eteks/sweethome3d/model/HomePieceOfFurniture.html

```
# get home furniture
window.application.getHomes()[0].furniture

# get piece of forniture from catalog
window.application.preferences.furnitureCatalog.getPieceOfFurnitureWithId("eTeks#shower")

window.application.preferences.furnitureCatalog.getCategories()[0].furniture[0]

# Create forniture
piece = new HomePieceOfFurniture(window.application.preferences.furnitureCatalog.getCategories()[0].furniture[0])

# position it
piece.setX(100)
piece.setY(100) 
piece.setElevation(100)

# add
window.application.getHomes()[0].addPieceOfFurniture(piece)
```

## Set Observer camera
http://www.sweethome3d.com/javadoc/com/eteks/sweethome3d/model/Camera.html
http://www.sweethome3d.com/javadoc/com/eteks/sweethome3d/model/ObserverCamera.html

```
# list cameras
window.application.getHomes()[0].camera

# move existing camera
window.application.getHomes()[0].camera.setX(0)
window.application.getHomes()[0].camera.setY(0)
window.application.getHomes()[0].camera.setZ(0)
window.application.getHomes()[0].camera.setYaw(0)
window.application.getHomes()[0].camera.setPitch(0)
window.application.getHomes()[0].camera.setFieldOfView(0)

# [Seams do not work] Create new camera
# x, y, z, yaw, pitch, fieldOfView
window.application.getHomes()[0].setCamera(new Camera(277, 1063, 1031, 3.14, 0.8, 1))
# or 
window.application.getHomes()[0].setCamera(new ObserverCamera(50, 50, 170, 5.5, 0.2, 1))

# Agirare con seguenti
# passare al Observer
document.getElementById("toolbar-button-VIEW_FROM_OBSERVER").click()
# passare al Ccamera normale
document.getElementById("toolbar-button-VIEW_FROM_TOP").click()
# now you can move existing camera
```

## Simple complete Script that create and destroy elements

```
# walls
window.application.getHomes()[0].addWall(new Wall(0, 0, 0, 300, 7.5, 250)) 

# room
room = new Room([[0,0], [0,300], [300, 300], [300, 0]])
room.setCeilingVisible(false)
window.application.getHomes()[0].addRoom(room)

# forniture
piece = new HomePieceOfFurniture(window.application.preferences.furnitureCatalog.getCategories()[0].furniture[0])
piece.setX(100)
piece.setY(100) 
window.application.getHomes()[0].addPieceOfFurniture(piece)

# delete all
window.application.getHomes()[0].walls.forEach(element => {
    window.application.getHomes()[0].deleteWall(element)
});
window.application.getHomes()[0].rooms.forEach(element => {
    window.application.getHomes()[0].deleteRoom(element)
});
window.application.getHomes()[0].furniture.forEach(element => {
    window.application.getHomes()[0].deletePieceOfFurniture(element)
});

```